import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import AddExpense from './components/AddExpense';
import AddIncome from './components/AddIncome';
import { GlobalProvider } from './context/GlobalState';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const App = () => {
  return (
    <div className='App'>
      <GlobalProvider>
        <Router>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/addexpense' component={AddExpense} />
            <Route path='/addincome' component={AddIncome} />
          </Switch>
        </Router>
      </GlobalProvider>
    </div>
  );
};

export default App;
