export default (state, action) => {
  switch (action.type) {
    case 'REMOVE_EXPENSE':
      return {
        transactions: state.transactions.filter((transaction) => {
          return transaction.id !== action.payload;
        }),
      };

    case 'ADD_TRANSACTION':
      return {
        transactions: [action.payload, ...state.transactions],
      };

    default:
      return state;
  }
};
