import React, { createContext, useReducer } from 'react';
import AppReducer from './AppReducer';

const initialState = {
  transactions: [
    {
      id: 1,
      desc: 'Groceries from Albert',
      amount: 1500.0,
      date: '2017-01-01',
      type: 'expense',
    },
    {
      id: 2,
      desc: 'Salary from Work',
      amount: 42000.0,
      date: '2017-08-01',
      type: 'income',
    },
    {
      id: 3,
      desc: 'New Shirt from ZARA',
      amount: 1500,
      date: '2017-01-01',
      type: 'expense',
    },
    {
      id: 4,
      desc: 'Icecream - It was Hot outside',
      amount: 13,
      date: '2017-01-01',
      type: 'expense',
    },
  ],
};

export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  //Actions
  const removeTransaction = (id) => {
    dispatch({
      type: 'REMOVE_EXPENSE',
      payload: id,
    });
  };

  const addTransaction = (transaction) => {
    dispatch({
      type: 'ADD_TRANSACTION',
      payload: transaction,
    });
  };

  return (
    <GlobalContext.Provider
      value={{
        transactions: state.transactions,
        removeTransaction,
        addTransaction,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
