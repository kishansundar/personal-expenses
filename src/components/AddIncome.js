import React, { useState, useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { GlobalContext } from '../context/GlobalState';
import { v4 as uuid } from 'uuid';

const AddIncome = () => {
  const { addTransaction } = useContext(GlobalContext);
  const history = useHistory();
  const [date, setDate] = useState('');
  const [amount, setAmount] = useState(0);
  const [desc, setDesc] = useState('');

  const onSubmit = () => {
    const newTransaction = {
      id: uuid(),
      desc,
      amount,
      date,
      type: 'income',
    };

    addTransaction(newTransaction);
    history.push('/');
  };
  return (
    <div>
      <h1>Add Income</h1>
      <Form onSubmit={onSubmit}>
        <FormGroup>
          <Label>Description:</Label>
          <Input
            placeholder='Enter Description'
            type='text'
            value={desc}
            onChange={(e) => setDesc(e.target.value)}
            required
          />
          <Label>Amount:</Label>
          <Input
            placeholder='Amount'
            type='number'
            value={amount}
            step='.01'
            onChange={(e) => setAmount(e.target.value)}
            required
          />
          <Label>Date:</Label>
          <Input
            type='date'
            value={date}
            onChange={(e) => setDate(e.target.value)}
            required
          />
        </FormGroup>
        <Button type='submit'>Add</Button>
        <Link to='/' className='btn btn-danger ml-2'>
          Cancel
        </Link>
      </Form>
    </div>
  );
};

export default AddIncome;
