import React, { useContext } from 'react';
import { Table, Navbar, Container } from 'reactstrap';
import { GlobalContext } from '../context/GlobalState';
import AddButtons from './AddButtons';

import Moment from 'moment';

const TransactionList = () => {
  const { transactions, removeTransaction } = useContext(GlobalContext);
  return (
    <div>
      <Table borderless>
        <tbody>
          {transactions.map((transaction) => {
            return (
              <tr>
                <td>{Moment(transaction.date).format('MM.DD.YYYY')}</td>
                {transaction.type === 'expense' && (
                  <td style={{ color: 'red' }}>
                    {new Intl.NumberFormat('en-GB', {
                      style: 'currency',
                      currency: 'INR',
                    }).format(transaction.amount)}
                  </td>
                )}
                {transaction.type === 'income' && (
                  <td style={{ color: 'green' }}>
                    {new Intl.NumberFormat('en-GB', {
                      style: 'currency',
                      currency: 'INR',
                    }).format(transaction.amount)}
                  </td>
                )}
                <td>{transaction.desc}</td>
                <td>
                  <button
                    style={{ border: 'none' }}
                    onClick={() => removeTransaction(transaction.id)}
                  >
                    <i className='fas fa-trash-alt'></i>
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>

      <Navbar color='faded'>
        <Container>
          <AddButtons
            name='Income'
            link='/addincome'
            classname='btn btn-success ml-2'
            exptype='income'
          />
          <AddButtons
            name='Spending'
            link='/addexpense'
            classname='btn btn-danger ml-2'
            exptype='expense'
          />
        </Container>
      </Navbar>
    </div>
  );
};

export default TransactionList;
