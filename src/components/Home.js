import React from 'react';
import Balance from './Balance';
import TransactionList from './TransactionList';

const Home = () => {
  return (
    <div>
      <Balance />
      <TransactionList />
    </div>
  );
};

export default Home;
