import React, { useContext } from 'react';
import { Navbar, NavbarBrand, Container } from 'reactstrap';
import { GlobalContext } from '../context/GlobalState';

const Balance = () => {
  const { transactions } = useContext(GlobalContext);

  const income = transactions
    .filter((transaction) => {
      return transaction.type === 'income';
    })
    .reduce((a, income) => {
      return a + parseFloat(income.amount);
    }, 0);

  const expense = transactions
    .filter((transaction) => {
      return transaction.type === 'expense';
    })
    .reduce((a, expense) => {
      return a + parseFloat(expense.amount);
    }, 0);

  let balance = income - expense;

  return (
    <div>
      <Navbar color='dark' dark>
        <Container>
          <NavbarBrand href='/'>
            <span
              style={{
                color: 'white',
              }}
            >
              Balance: {balance}
            </span>
          </NavbarBrand>
          <NavbarBrand href='/'>
            <span
              style={{
                color: 'greenyellow',
              }}
            >
              Income: {income}
            </span>
          </NavbarBrand>
          <NavbarBrand href='/'>
            <span
              style={{
                color: 'red',
              }}
            >
              Spendings: {expense}
            </span>
          </NavbarBrand>
        </Container>
      </Navbar>
    </div>
  );
};

export default Balance;
