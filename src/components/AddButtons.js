import React from 'react';
import { Link } from 'react-router-dom';
import { NavbarBrand } from 'reactstrap';

function AddButtons(props) {
  return (
    <div>
      <NavbarBrand>
        <Link
          to={props.link}
          className={props.classname}
          exptype={props.exptype}
        >
          Add {props.name}
        </Link>
      </NavbarBrand>
    </div>
  );
}

export default AddButtons;
