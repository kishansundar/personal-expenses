## Getting Started

#### Prerequisites

[Node.js](https://nodejs.org/en/)
[Yarn](https://yarnpkg.com/en/)

- install all project dependencies with `yarn install`
- start the development server with `yarn start`

## Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). You can find more information on how to perform common tasks [here](https://github.com/facebook/create-react-app/blob/master/packages/cra-template/template/README.md).
